import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Frame extends JFrame {

    private List<Point> points = new ArrayList<>();

    public Frame() {
        int w = 800;
        int h = 600;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(w, h);
        setLocation(300, 300);
        JPanel content = (JPanel) getContentPane();
        render();

        setResizable(false);

        JPanel diagramPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(Color.red);
                g.drawLine(w / 2, 0, w / 2, h);
                g.drawLine(0, h / 2, w, h / 2);
                g.setColor(Color.black);
                for (Point point : points) {
                    g.fillOval(point.x + w / 2, point.y + h / 2, 2, 2);
                }

            }
        };

        content.add(diagramPanel);
        setVisible(true);

    }

    private void render() {
        for (double i = 1; i < 1000; i += 0.01) {
            points.add(new Point((int) i, -(int) (50 * func(i / 50.0))));
            points.add(new Point((int) (-i), -(int) (50 * func(-i / 50.0))));
        }
        repaint();
    }

    private double func(double x) {
        //type your func here

        return (x) * Math.sin(20 / x);
    }
}
